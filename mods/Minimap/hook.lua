local hooks = {
	["minimap"] = {
		"lib/managers/hudmanagerpd2",
		"lib/managers/criminalsmanager",
		"lib/units/beings/player/huskplayermovement",
		"lib/units/contourext",
		"lib/managers/hudmanager"
	}
}

for file, hook in pairs(hooks) do
	for _, path in pairs(hook) do
		Hook(path, string.format("LightHook/minimap/%s", file))
	end
end